def count_label(li_ann):
    nkli = {}
  for ann in li_ann:
      nkli[ann.event_fn_id] = nkli.get(ann.event_fn_id, 0) + 1
  return nkli



def pe(l1_ann, l2_ann):
    nkl1 = count_label(l1_ann)
    nkl2 = count_label(l2_ann)
    
  nks = set(nkl1.keys()).union(set(nkl2.keys()))

  return sum(nkl1.get(k,0) * nkl2.get(k,0) for k in nks) / len(l1_ann)**2


def po(l1_ann, l2_ann):
    l1_ann_by_id = {ann.event_id : ann.event_fn_id for ann in l1_ann}
    l2_ann_by_id = {ann.event_id : ann.event_fn_id for ann in l2_ann}

  observed_count = 0  
  for id, event_fn_id in l1_ann_by_id.items():
    if l2_ann_by_id[id] == event_fn_id:
        observed_count += 1

  return observed_count/len(l1_ann)



def checking_labelling(annotator1, annotator2):
    events_intersection = intersect_events(annotator1['ann'], annotator2['ann'])
    pe_value = pe(events_intersection, annotator2['ann'].values())
    po_value = po(events_intersection, annotator2['ann'].values())
    k = (po_value - pe_value) / (1 - pe_value)
    print('Annotator 1: \t%s \nAnnotator 2: \t%s \npe: %s, po: %s, k: %s' % (annotator1['name'], annotator2['name'], pe_value, po_value, k))

  events_diff = []
  for event_ann in events_intersection:
    if annotator2['ann'][event_ann.event_id].event_fn_id != event_ann.event_fn_id:
        events_diff.append(event_ann.event_id)
  return {'annotator_1': annotator1,
          'annotator_2': annotator2,
          'events_ids' : events_diff}




def f1(p,r): 
  if(p+r) > 0:
    return (2*p*r)/(p+r) 
  else:
    return 0


def f1_evaluation(gold_ann, sys_ann):
    classes_metrics =  {ann.event_fn_id : {'tp':0, 'fp':0, 'fn':0} for ann in gold_ann}
  for classe in classes_metrics:
      classes_metrics[classe]['ids'] = {ann.event_id for ann in gold_ann if ann.event_fn_id == classe}
      gold_classe_ids = classes_metrics[classe]['ids']
      sys_classe_ids = {ann.event_id for ann in sys_ann if ann.event_fn_id == classe}

    classes_metrics[classe]['tp_ids'] = gold_classe_ids.intersection(sys_classe_ids)
    classes_metrics[classe]['tp'] = len(classes_metrics[classe]['tp_ids'])
    classes_metrics[classe]['fp_ids'] = sys_classe_ids.difference(gold_classe_ids)
    classes_metrics[classe]['fp'] = len(classes_metrics[classe]['fp_ids'])
    classes_metrics[classe]['fn_ids'] = gold_classe_ids.difference(sys_classe_ids)
    classes_metrics[classe]['fn'] = len(classes_metrics[classe]['fn_ids'])

    tp = classes_metrics[classe]['tp']
    fp = classes_metrics[classe]['fp']
    fn = classes_metrics[classe]['fn']
    tp_fp = tp + fp
    tp_fn = tp + fn
    p = classes_metrics[classe]['p'] = tp / (tp_fp if tp_fp > 0 else 1)
    r = classes_metrics[classe]['r'] = tp / (tp_fn if tp_fn > 0 else 1)
    classes_metrics[classe]['f1'] = f1(p, r)

  sys_eval_output = {'by_class' : classes_metrics, 'micro-avg' : {}, 'macro-avg': {} }

  all_tp = sum([classes_metrics[classe]['tp'] for classe in classes_metrics])
  all_fp = sum([classes_metrics[classe]['fp'] for classe in classes_metrics])
  all_fn = sum([classes_metrics[classe]['fn'] for classe in classes_metrics])

  all_tp_fp = (all_tp + all_fp)
  all_tp_fn = (all_tp + all_fn)
  micro_avg_p = sys_eval_output['micro-avg']['p'] = all_tp / all_tp_fp if all_tp_fp > 0 else 1
  micro_avg_r = sys_eval_output['micro-avg']['r'] = all_tp / all_tp_fn if all_tp_fn > 0 else 1
  sys_eval_output['micro-avg']['f1'] = f1(micro_avg_p, micro_avg_r)

  all_p = sum([classes_metrics[classe]['p'] for classe in classes_metrics])
  all_r = sum([classes_metrics[classe]['r'] for classe in classes_metrics])
  macro_avg_p = sys_eval_output['macro-avg']['p'] = all_p / len(classes_metrics)
  macro_avg_r = sys_eval_output['macro-avg']['r'] = all_r / len(classes_metrics)
  sys_eval_output['macro-avg']['f1'] = f1(macro_avg_p, macro_avg_r)

  return sys_eval_output
