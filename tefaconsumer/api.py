from tefaconsumer.db import *
import glob
import csv
import nltk
from nltk.corpus import framenet as fn
from sqlalchemy import func
from sqlalchemy import and_
from sqlalchemy import select
from sqlalchemy import distinct

nltk.download('framenet_v17')



def load_db(dbpath):
    create_session(dbpath)
    
def load_events(annotator_name, annotation_status='done'):
    "Load events annotations from annotators_data dict and annotator_name and annotation_status"
    events_ann = load_events_ann(annotator_name, annotation_status)
    events_ann = {'ann': {event_ann.event_id : event_ann for event_ann in events_ann}}
    events_ann['name'] = annotator_name
    return events_ann

def intersect_events(events_l1, events_l2):
    return [event_ann for (event_id, event_ann) in  events_l1.items() if event_id in events_l2 ]

def test_s():
    with session_scope() as session:
        return session.query(distinct(EventANN.event_fn_id)).\
            join(EventTBPT, EventANN.event_id==EventTBPT.id).\
            join(Sentence, Sentence.id==EventTBPT.sentence_id).\
            filter(Sentence.partition_name=='test').\
            all()
            
def train_r():
    with session_scope() as session:
        return session.query(distinct(EventANN.event_fn_id)).\
            join(EventTBPT, EventANN.event_id==EventTBPT.id).\
            join(Sentence, Sentence.id==EventTBPT.sentence_id).\
            filter(Sentence.partition_name=='train').\
            all()

def events_more_than(more_than=0, partition_name='train',partitions=['train','test']):
    with session_scope() as session:
        q_test = session.query(distinct(EventANN.event_fn_id)).\
            join(EventTBPT, EventANN.event_id==EventTBPT.id).\
            join(Sentence, Sentence.id==EventTBPT.sentence_id).\
            filter(Sentence.partition_name=='test')

        q_train = session.query(distinct(EventANN.event_fn_id)).\
            join(EventTBPT, EventANN.event_id==EventTBPT.id).\
            join(Sentence, Sentence.id==EventTBPT.sentence_id).\
            filter(Sentence.partition_name=='train')

    
        return session.query(EventANN.event_fn_id,
                             func.count(EventANN.id)).\
                             join(EventTBPT, EventANN.event_id==EventTBPT.id).\
                             join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                             filter(Sentence.partition_name==partition_name,
                                    EventANN.event_fn_id.in_(q_test.intersect(q_train))).\
                            group_by(EventANN.event_fn_id).\
                            having(func.count(EventANN.id) > more_than).\
                            order_by(EventANN.event_fn_id).all()
                         
def sentences_count_by_event_fn_ids(event_fn_ids, partition_name=None, nevents='all'):
    with session_scope() as session:
        if partition_name and nevents == 'one':
            out = session.query(func.count(distinct(Sentence.id))).\
                           join(EventTBPT, EventTBPT.sentence_id==Sentence.id).\
                           join(EventANN, EventTBPT.id==EventANN.event_id).\
                              filter(Sentence.partition_name==partition_name,
                                 EventANN.event_fn_id.in_(event_fn_ids)).\
                            group_by(Sentence.id).\
                            having(func.count(Sentence.id)==1).\
                            all()
        elif partition_name and nevents == 'n':
            out = session.query(func.count(distinct(Sentence.id))).\
                           join(EventTBPT, EventTBPT.sentence_id==Sentence.id).\
                           join(EventANN, EventTBPT.id==EventANN.event_id).\
                              filter(Sentence.partition_name==partition_name,
                                 EventANN.event_fn_id.in_(event_fn_ids)).\
                            group_by(Sentence.id).\
                            having(func.count(Sentence.id)>1).\
                            all()
        elif partition_name and nevents == 'all':
            out = session.query(func.count(distinct(Sentence.id))).\
                           join(EventTBPT, EventTBPT.sentence_id==Sentence.id).\
                           join(EventANN, EventTBPT.id==EventANN.event_id).\
                              filter(Sentence.partition_name==partition_name,
                                 EventANN.event_fn_id.in_(event_fn_ids)).\
                            group_by(Sentence.id).\
                            having(func.count(Sentence.id)>0).\
                            all()            
        else:
            out = session.query(func.count(Sentence.id)).\
                join(EventTBPT, EventTBPT.sentence_id==Sentence.id).\
                join(EventANN, EventTBPT.id==EventANN.event_id).\
                filter(EventANN.event_fn_id.in_(event_fn_ids)).\
                       first()
        if out:
            return sum([row[0] for row in out])




def events_ann_count_by_event_fn_ids(event_fn_ids, partition_name=None, nevents='all'):
    with session_scope() as session:
        if partition_name and nevents == 'one':
            one_event_sents = session.query(distinct(Sentence.id)).\
                           join(EventTBPT, EventTBPT.sentence_id==Sentence.id).\
                           join(EventANN, EventTBPT.id==EventANN.event_id).\
                              filter(Sentence.partition_name==partition_name,
                                 EventANN.event_fn_id.in_(event_fn_ids)).\
                            group_by(Sentence.id).\
                            having(func.count(Sentence.id)==1)

            out = session.query(func.count(EventANN.id)).\
                join(EventTBPT, EventTBPT.id==EventANN.event_id).\
                join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                filter(Sentence.partition_name==partition_name,
                       EventANN.event_fn_id.in_(event_fn_ids),
                       Sentence.id.in_(one_event_sents)).\
                       all()
        elif partition_name and nevents == 'n':
            n_event_sents = session.query(distinct(Sentence.id)).\
                           join(EventTBPT, EventTBPT.sentence_id==Sentence.id).\
                           join(EventANN, EventTBPT.id==EventANN.event_id).\
                              filter(Sentence.partition_name==partition_name,
                                 EventANN.event_fn_id.in_(event_fn_ids)).\
                            group_by(Sentence.id).\
                            having(func.count(Sentence.id)>1)


            out = session.query(func.count(EventANN.id)).\
                join(EventTBPT, EventTBPT.id==EventANN.event_id).\
                join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                filter(Sentence.partition_name==partition_name,
                       EventANN.event_fn_id.in_(event_fn_ids),
                       Sentence.id.in_(n_event_sents)).\
                       all()
        elif partition_name and nevents == 'all':
            out = session.query(func.count(EventANN.id)).\
                join(EventTBPT, EventTBPT.id==EventANN.event_id).\
                join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                filter(Sentence.partition_name==partition_name,
                       EventANN.event_fn_id.in_(event_fn_ids)).\
                       all()                      
        else:
            out = session.query(func.count(EventANN.id)).\
                join(EventTBPT, EventTBPT.id==EventANN.event_id).\
                join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                filter(EventANN.event_fn_id.in_(event_fn_ids)).\
                all()
        if out:
            return sum([row[0] for row in out])

        
def args_more_than(more_than, event_ids=None, partition_name='train'):
    with session_scope() as session:
        q_test = session.query(ArgANN.event_fe_id).\
                               distinct().\
                               join(EventANN, EventANN.id==ArgANN.event_ann_id).\
                               join(EventTBPT, EventANN.event_id==EventTBPT.id).\
                               join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                               filter(Sentence.partition_name=='test')

        q_train = session.query(ArgANN.event_fe_id).\
                               distinct().\
                               join(EventANN, EventANN.id==ArgANN.event_ann_id).\
                               join(EventTBPT, EventANN.event_id==EventTBPT.id).\
                               join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                               filter(Sentence.partition_name=='train')

        if event_ids:
            return session.query(ArgANN.event_fe_id,
                                 EventANN.event_fn_id,
                     func.count(ArgANN.event_fe_id)).\
                     distinct().\
                     join(EventANN, EventANN.id==ArgANN.event_ann_id).\
                     join(EventTBPT, EventANN.event_id==EventTBPT.id).\
                     join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                     filter(Sentence.partition_name==partition_name,
                            ArgANN.event_fe_id.in_(q_test.intersect(q_train)),
                            EventANN.event_fn_id.in_(event_ids)).\
                            group_by(ArgANN.event_fe_id).\
                            having(func.count(ArgANN.event_fe_id) > more_than).\
                            order_by(ArgANN.event_fe_id).all()
        else:
            return session.query(ArgANN.event_fe_id,
                                 EventANN.event_fn_id,
                             func.count(ArgANN.event_fe_id)).\
                      distinct().\
                      join(EventANN, EventANN.id==ArgANN.event_ann_id).\
                      join(EventTBPT, EventANN.event_id==EventTBPT.id).\
                      join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                      filter(Sentence.partition_name==partition_name,
                             ArgANN.event_fe_id.in_(q_test.intersect(q_train))).\
                      group_by(ArgANN.event_fe_id).\
                      having(func.count(ArgANN.event_fe_id) > more_than).\
                      order_by(ArgANN.event_fe_id).all()

def args_ann_count_by(event_fe_ids=None, event_fn_ids=None, partition_name=None, nevents='all'):
    with session_scope() as session:
        out = None

        if nevents == 'one':
            one_event_sents = session.query(distinct(Sentence.id)).\
                           join(EventTBPT, EventTBPT.sentence_id==Sentence.id).\
                           join(EventANN, EventTBPT.id==EventANN.event_id).\
                              filter(Sentence.partition_name==partition_name,
                                 EventANN.event_fn_id.in_(event_fn_ids)).\
                            group_by(Sentence.id).\
                            having(func.count(Sentence.id)==1)
        elif nevents == 'n':
            n_event_sents = session.query(distinct(Sentence.id)).\
                           join(EventTBPT, EventTBPT.sentence_id==Sentence.id).\
                           join(EventANN, EventTBPT.id==EventANN.event_id).\
                              filter(Sentence.partition_name==partition_name,
                                 EventANN.event_fn_id.in_(event_fn_ids)).\
                            group_by(Sentence.id).\
                            having(func.count(Sentence.id)>1)
            
        if event_fe_ids and event_fn_ids and partition_name:
            if nevents == 'one':
                out = session.query(ArgANN.start_at,
                                ArgANN.end_at,
                                ArgANN.event_fe_id,
                                ArgANN.event_ann_id).\
                                distinct().\
                                join(EventANN, EventANN.id==ArgANN.event_ann_id).\
                                join(EventTBPT, EventTBPT.id==EventANN.event_id).\
                                join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                                filter(Sentence.partition_name==partition_name,
                                       ArgANN.event_fe_id.in_(event_fe_ids),
                                       EventANN.event_fn_id.in_(event_fn_ids),
                                       Sentence.id.in_(one_event_sents)).\
                                       count()
            elif nevents == 'n':
                out = session.query(ArgANN.start_at,
                                ArgANN.end_at,
                                ArgANN.event_fe_id,
                                ArgANN.event_ann_id).\
                                distinct().\
                                join(EventANN, EventANN.id==ArgANN.event_ann_id).\
                                join(EventTBPT, EventTBPT.id==EventANN.event_id).\
                                join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                                filter(Sentence.partition_name==partition_name,
                                       ArgANN.event_fe_id.in_(event_fe_ids),
                                       EventANN.event_fn_id.in_(event_fn_ids),
                                       Sentence.id.in_(n_event_sents)).\
                                       count()
            else:
                out = session.query(ArgANN.start_at,
                                ArgANN.end_at,
                                ArgANN.event_fe_id,
                                ArgANN.event_ann_id).\
                                distinct().\
                                join(EventANN, EventANN.id==ArgANN.event_ann_id).\
                                join(EventTBPT, EventTBPT.id==EventANN.event_id).\
                                join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                                filter(Sentence.partition_name==partition_name,
                                       ArgANN.event_fe_id.in_(event_fe_ids),
                                       EventANN.event_fn_id.in_(event_fn_ids)).\
                                       count()
        elif event_fe_ids and partition_name:
            out = session.query(ArgANN.start_at,
                                ArgANN.end_at,
                                ArgANN.event_fe_id,
                                ArgANN.event_ann_id).\
                                distinct().\
                                join(EventANN, EventANN.id==ArgANN.event_ann_id).\
                                join(EventTBPT, EventTBPT.id==EventANN.event_id).\
                                join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                                filter(Sentence.partition_name==partition_name,
                                       ArgANN.event_fe_id.in_(event_fe_ids)).\
                                       count()
        elif event_fn_ids and partition_name:
            out = session.query(ArgANN.start_at,
                                ArgANN.end_at,
                                ArgANN.event_fe_id,
                                ArgANN.event_ann_id).\
                                distinct().\
                                join(EventANN, EventANN.id==ArgANN.event_ann_id).\
                                join(EventTBPT, EventTBPT.id==EventANN.event_id).\
                                join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                                filter(Sentence.partition_name==partition_name,
                                       EventANN.event_fn_id.in_(event_fn_ids)).\
                                       count()            
        elif event_fe_ids:
            out = session.query(ArgANN.start_at,
                                ArgANN.end_at,
                                ArgANN.event_fe_id,
                                ArgANN.event_ann_id).\
                                distinct().\
                                filter(ArgANN.event_fe_id.in_(event_fe_ids)).\
                                       count()
        elif partition_name:
            out = session.query(ArgANN.start_at,
                                ArgANN.end_at,
                                ArgANN.event_fe_id,
                                ArgANN.event_ann_id).\
                                distinct().\
                                join(EventANN, EventANN.id==ArgANN.event_ann_id).\
                                join(EventTBPT, EventTBPT.id==EventANN.event_id).\
                                join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                                filter(Sentence.partition_name==partition_name).\
                                count()

        elif event_fn_ids:
            out = session.query(ArgANN.start_at,
                                ArgANN.end_at,
                                ArgANN.event_fe_id,
                                ArgANN.event_ann_id).\
                                distinct().\
                                join(EventANN, EventANN.id==ArgANN.event_ann_id).\
                                filter(EventANN.event_fn_id.in_(event_fn_ids)).\
                                       count()            
        return out

        
def events_from_sentence(document_name, text):
    events_ann = []
    with session_scope() as session:
        events_ann = session.query(EventANN).\
            join(EventTBPT, EventANN.event_id==EventTBPT.id).\
            join(Sentence, Sentence.id==EventTBPT.sentence_id).\
            filter(Sentence.document_name==document_name,
                   Sentence.text==text).\
            all()
        events_tbpt_ids = [event_ann.event_id for event_ann in events_ann]
        events_tbpt = session.query(EventTBPT).\
            filter(EventTBPT.id.in_(events_tbpt_ids)).\
            all()
    return events_ann, events_tbpt


def load_events_ann(annotator_id, status):
    events_ann = []
    with session_scope() as session:
        events_ann = session.query(EventANN).\
            join(EventTBPT, EventANN.event_id==EventTBPT.id).\
            join(Sentence, Sentence.id==EventTBPT.sentence_id).\
            join(SentenceAnnotator, Sentence.id==SentenceAnnotator.sentence_id).\
            join(Annotator, Annotator.email==SentenceAnnotator.annotator_id).\
            filter(SentenceAnnotator.status==status,
                   SentenceAnnotator.annotator_id==annotator_id).\
                   all()
    return events_ann

def args_from_event_ann(event_ann_id):
    events_args = []
    with session_scope() as session:
        events_args = session.query(ArgANN).\
            filter(ArgANN.event_ann_id==event_ann_id).\
            all()
    return events_args

def all_args_event(filter_ids=None):
    all_args = []
    with session_scope() as session:
        if not filter_ids:
            all_args = session.query(ArgANN).\
                all()
        else:
            all_args = session.query(ArgANN).\
                join(EventANN, ArgANN.event_ann_id==EventANN.id).\
                filter(EventANN.event_fn_id.in_(filter_ids)).\
                all()
                
    return all_args



def get_trigger_count_by(event_type_name, partition_name='train'):
    trigger_dict = {}
    event_type_id = frame_by_name(event_type_name).ID
    with session_scope() as session:
        for trigger, count in  session.query(EventTBPT.trigger,
                                             func.count(EventTBPT.trigger)).\
                                             join(EventANN, EventANN.event_id==EventTBPT.id).\
                                             join(Sentence, Sentence.id==EventTBPT.sentence_id).\
                                             filter(Sentence.partition_name==partition_name,
                                                    EventANN.event_fn_id==event_type_id).\
                                             group_by(EventTBPT.trigger).all():
            trigger_dict[trigger.lower()] = trigger_dict.get(trigger.lower(),0) + count
    return trigger_dict

    
def get_event_ann(event_ann_id):
    event_ann = None
    with session_scope() as session:
        event_ann = session.query(EventANN).\
            filter(EventANN.id==event_ann_id).\
            first()
    return event_ann

def group_args_by_name(args_ann):
    type_name_dict = {}
    for arg_ann in args_ann:
        event_ann = get_event_ann(arg_ann.event_ann_id)
        fes = fes_by_id_dict(event_ann.event_fn_id)
        arg_type_name = fes[arg_ann.event_fe_id]
        type_name_dict[arg_type_name] = type_name_dict.get(arg_type_name, 0) + 1
    return type_name_dict
        
def fes_by_id_dict(frame_id):
    frame = frame_by_id(frame_id)
    fes_dict = {}
    for name, fe in frame.FE.items():
        fes_dict[fe.ID] = frame.name +'#'+ name
    return fes_dict

def get_all_args_event_name_dict():
    type_name_id_dict = {}
    for arg_ann in all_args_event():
        event_ann = get_event_ann(arg_ann.event_ann_id)
        fes = fes_by_id_dict(event_ann.event_fn_id)
        arg_type_name = fes[arg_ann.event_fe_id]
        type_name_id_dict[arg_type_name] = arg_ann.event_fe_id
    return type_name_id_dict
    
def frame_by_id(frame_id):
    return fn.frame(frame_id)

def frame_by_name(frame_name):
    return fn.frame(frame_name)

def frame_id(frame):
    return frame.ID

def event_types_id_index(event_types):
    return {frame_id(frame_by_name(event_type)):i+1 for i, event_type in enumerate(sorted(event_types))}


def arg_types_id_index(arg_types):
    args_event_name_id = get_all_args_event_name_dict()
    return {args_event_name_id[arg_type]:i+1 for i, arg_type in enumerate(sorted(arg_types))}


def query_sentences_by_document(document):
    sentences = None
    with session_scope() as session:
        sentences = session.query(Sentence).\
            filter(Sentence.document_name==document).\
            all()
    return sentences

def create_doc_sentences_txt(dirpathname, docname):
    with open('%s%s.txt' % (dirpathname, docname), 'w') as out:
        for sentence in query_sentences_by_document(docname):
            out.write('%s\n' % sentence.text)


def persist_sentences_by_partition_name(partition_name, dirpathname):
    with session_scope() as session:
        for docname in session.query(Sentence.document_name).\
                                distinct().\
                                group_by(Sentence.document_name).\
                                where(Sentence.partition_name==partition_name).\
                                all():
            create_doc_sentences_txt(dirpathname, docname[0])


    
def query_sentence_by_event_id(id):
    sentence = None
    with session_scope() as session:
        sentence = session.query(Sentence).\
            join(EventTBPT, Sentence.id==EventTBPT.sentence_id).\
            filter(EventTBPT.id==id).\
            first()
    return sentence

def query_event_tbpt(event_id):
    event = None
    with session_scope() as session:
        event = session.query(EventTBPT).filter_by(id=event_id).first()
    return event

def group_ann_by_name(events_ann):
    type_name_dict = {}
    for event_ann in events_ann:
        type_name = frame_by_id(event_ann.event_fn_id).name
        type_name_dict[type_name] = type_name_dict.get(type_name, 0) + 1
    return type_name_dict

def get_info_diff_ann(diff, is_print_diff=False):
    diff_info = {'annotator_1' : diff['annotator_1'], 'annotator_2': diff['annotator_2']}
    diff_info['events_diff'] = {}
    annotator1 = diff['annotator_1']
    annotator2 = diff['annotator_2']
    for event_id in diff['events_ids']:
        event_diff = {}
        diff_info['events_diff'][event_id] = event_diff
        sentence = query_sentence_by_event_id(event_id).text
        event_tbpt = query_event_tbpt(event_id)
        event_ann1 = annotator1['ann'][event_id]
        event_ann2 = annotator2['ann'][event_id]

        event_diff['sentence'] = sentence
        event_diff['trigger'] = event_tbpt.trigger
        event_diff['type1'] = frame_by_id(event_ann1.event_fn_id).name
        event_diff['type2'] = frame_by_id(event_ann2.event_fn_id).name

        if is_print_diff:          
            print('\n\n')
            print('Sentença: %s' %  event_diff['sentence'])
            print('Evento: %s' % event_diff['trigger'])
            print('%s Tipo: %s' % (annotator1['name'], event_diff['type1']))
            print('%s Tipo: %s' % (annotator2['name'], event_diff['type2']))
        
    return diff_info




def persist_diff(diff_info, week_name, week_path):
  with open(week_path + week_name + '/diff.csv', 'w', newline='') as csvfile:
      keys = ['event_id', 'sentence', 'trigger', 'type1', 'type2']
      writer = csv.DictWriter(csvfile, fieldnames=keys)
      writer.writeheader()
      for event_id, event_diff in diff_info['events_diff'].items():
          writer.writerow({**{'event_id': event_id},**event_diff})
