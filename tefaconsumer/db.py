import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import sessionmaker
import uuid
from contextlib import contextmanager


@contextmanager
def session_scope():
    """Provide a transactional scope around a series of operations."""
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()

def get_engine():
    return engine

def create_session(dbpath='lome_tbpt.db'):
    global Session
    global engine
    
    engine = create_engine('sqlite:///%s' % (dbpath),  connect_args={'check_same_thread': False}, echo=False)
    Base.metadata.create_all(engine)
    
    Session = sessionmaker(bind=engine, expire_on_commit=False)


Base = declarative_base()



class Annotator(Base):
    __tablename__ = 'annotator'

    email = Column(String, primary_key=True)
    created_at = Column(DateTime)
    posted_at = Column(DateTime)
    

class Sentence(Base):
    __tablename__ = 'sentence'

    id = Column(String, primary_key=True)
    text = Column(String)
    document_name = Column(String)
    position = Column(Integer)
    partition_name = Column(String)


class SentenceAnnotator(Base):
    __tablename__ = 'sentence_annotator'

    status = Column(String)
    annotator_id = Column(String, ForeignKey('annotator.email'), primary_key=True)
    sentence_id = Column(String, ForeignKey('sentence.id'), primary_key=True)


class EventTBPT(Base):
    __tablename__ = 'event_tbpt'

    id = Column(String, primary_key=True)
    eid = Column(String)
    trigger = Column(String)
    lemma = Column(String)
    pos = Column(String)
    start_at = Column(Integer)
    end_at = Column(Integer)

    sentence_id = Column(String, ForeignKey('sentence.id'))



class EventANN(Base):
    __tablename__ = 'event_ann'

    id = Column(String, primary_key=True)
    event_fn_id = Column(Integer)
    event_id = Column(String, ForeignKey('event_tbpt.id'))
    created_at = Column(DateTime)
    posted_at = Column(DateTime)
    updated_at = Column(DateTime)
    annotator_id = Column(String, ForeignKey('annotator.email'))


class ArgANN(Base):
    __tablename__ = 'arg_ann'
    
    start_at = Column(Integer, primary_key=True)
    end_at = Column(Integer, primary_key=True)
    created_at = Column(DateTime)
    posted_at = Column(DateTime)
    updated_at = Column(DateTime)
    event_fe_id = Column(Integer, primary_key=True)
    event_ann_id = Column(String, ForeignKey('event_ann.id'), primary_key=True)
    annotator_id = Column(String, ForeignKey('annotator.email'), primary_key=True)
